# MonitSecArc
MontiSecArc (MSA) is a Security Architecture Description Language. This repository contains tooling based on MontiCore for Security Architecture Analysis and development of generators to derive code from MSA models.

# Licenses
* [LGPL V3.0](https://github.com/MontiCore/monticore/tree/master/00.org/Licenses/LICENSE-LGPL.md) (for handwritten Java code)
* [BSD-3-Clause](https://github.com/MontiCore/monticore/tree/master/00.org/Licenses/LICENSE-BSD3CLAUSE.md) (for templates and generated Java code)

# Content Overview
Folders in this repository:
* core     - the MontiSecArc language and analysis tools source code
* doc      - documentation for MontiSecArc
* examples - example projects for the MontiSecArc tools  <------------ start here

# Getting Started
Try out the pre-compiled Architecture Analysis by running:
```
cd examples/montiSecArcAnalysis/
./startExample.sh
```

# Build
Make sure you have access to MontiCore version 3. Then run the maven build:
```
cd core
mvn clean install
```
