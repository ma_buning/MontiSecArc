#MontiSecArc Analysis Example
Start the analysis tool by executing the script startExample.sh.
This will analyze the MontiSecArc Model in src/test/resources/secarc/seccds/fe/Store.secarc, which uses different files in src/test/resources/.

The impact of the individual analysis can be adjusted in src/main/conf/Analysis_Conf.txt

#Build
In case you want to change the analysises implemented in MontiSecArcAnalysis, run mvn install in the ../../core folder and change the script startExample.sh to point at the resulting jar ../../core/montiSecArcAnalysis/target/montiSecArcAnalysis.jar.
