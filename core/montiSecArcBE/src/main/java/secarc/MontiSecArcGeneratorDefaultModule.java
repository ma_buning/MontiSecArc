package secarc;

import interfaces2.language.ModelingLanguage;


/**
 * Default guice module for MontiSecArc generator.
 *
 * @author  (last commit) $Author$
 * @version $Revision$,
 *          $Date$
 *
 */
public class MontiSecArcGeneratorDefaultModule extends MontiSecArcDefaultModule {

  /*@Override
  protected void bindGenerator() {
    bind(ModelingLanguage.class).to(MontiSecArcGeneratorLanguage.class);
  }*/
	
}
