package secarc.ets.analysis.checker;

import secarc._ast.ASTSecArcConfiguration;
import secarc.ets.entries.ConfigurationEntry;

/**
 * Analysis checker interface for checking configuration
 * related analysis
 * 
 * <br>
 * <br>
 * Copyright (c) 2011 RWTH Aachen. All rights reserved
 * 
 * @author (last commit) $Author$
 * @version $Date$<br>
 * $Revision$
 * 
 */
public interface ISecAnalysisConfigurationChecker {

	/**
	 * Checks analysis of configuration
	 * 
	 * @param node ast node to check
	 * @param entry entry of ast node
	 * @param advanced detailness of output
	 * @return factor of analysis
	 */
	int check(ASTSecArcConfiguration node, ConfigurationEntry entry, boolean advanced);
	
}
