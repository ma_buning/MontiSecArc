package secarc.ets.analysis.checker;

import interfaces2.coco.ContextCondition;

/**
 * Analysis are special context conditions
 * 
 * <br>
 * <br>
 * Copyright (c) 2011 RWTH Aachen. All rights reserved
 * 
 * @author (last commit) $Author$
 * @version $Date$<br>
 * $Revision$
 * 
 */
public abstract class Analysis extends ContextCondition {

	public Analysis(String checkedProperty) {
		super(checkedProperty);
	}

}
