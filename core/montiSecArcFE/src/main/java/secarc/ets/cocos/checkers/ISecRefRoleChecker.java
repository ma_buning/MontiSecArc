package secarc.ets.cocos.checkers;

import interfaces2.resolvers.AmbigousException;
import secarc._ast.ASTSecArcRefRole;

/**
 * Context condition checker interface for checking port roles
 * related context conditions
 * 
 * <br>
 * <br>
 * Copyright (c) 2011 RWTH Aachen. All rights reserved
 * 
 * @author (last commit) $Author$
 * @version $Date$<br>
 * $Revision$
 * 
 */
public interface ISecRefRoleChecker {

	/**
	 * 
	 * @param node
	 * @param entry
	 * @throws AmbigousException 
	 */
	void check(ASTSecArcRefRole node) throws AmbigousException;
	
}
