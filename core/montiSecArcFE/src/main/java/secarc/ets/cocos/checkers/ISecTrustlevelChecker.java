package secarc.ets.cocos.checkers;

/**
 * Context condition checker interface for checking trustlevel
 * related context conditions
 * 
 * <br>
 * <br>
 * Copyright (c) 2011 RWTH Aachen. All rights reserved
 * 
 * @author (last commit) $Author$
 * @version $Date$<br>
 * $Revision$
 * 
 */
public interface ISecTrustlevelChecker {

}
