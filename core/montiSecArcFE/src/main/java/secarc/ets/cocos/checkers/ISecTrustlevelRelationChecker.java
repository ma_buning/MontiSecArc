package secarc.ets.cocos.checkers;

import interfaces2.resolvers.AmbigousException;
import secarc._ast.ASTSecArcTrustlevelRelation;
import secarc.ets.entries.TrustlevelRelationEntry;

/**
 * Context condition checker interface for checking trustlevel relation
 * related context conditions
 * 
 * <br>
 * <br>
 * Copyright (c) 2011 RWTH Aachen. All rights reserved
 * 
 * @author (last commit) $Author$
 * @version $Date$<br>
 * $Revision$
 * 
 */
public interface ISecTrustlevelRelationChecker {
	
	/**
	 * 
	 * @param node ast node to be checked
	 * @param entry entry related trustlevel relation entry to be checked
	 */
	void check(ASTSecArcTrustlevelRelation node, TrustlevelRelationEntry entry) throws AmbigousException;

}
