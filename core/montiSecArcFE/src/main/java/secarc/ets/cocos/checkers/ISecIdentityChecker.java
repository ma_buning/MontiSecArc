package secarc.ets.cocos.checkers;

import interfaces2.helper.EntryLoadingErrorException;
import interfaces2.resolvers.AmbigousException;
import secarc._ast.ASTSecArcIdentity;
import secarc.ets.entries.IdentityEntry;
import secarc.ets.graph.ArchitectureGraph;

/**
 * Context condition checker interface for checking identity
 * related context conditions
 * 
 * <br>
 * <br>
 * Copyright (c) 2011 RWTH Aachen. All rights reserved
 * 
 * @author (last commit) $Author$
 * @version $Date$<br>
 * $Revision$
 * 
 */
public interface ISecIdentityChecker {

	/**
	 * 
	 * @param node ast node to be checked
	 * @param entry entry related identity entry to be checked
	 * @throws AmbigousException 
	 */
	void check(ASTSecArcIdentity node, IdentityEntry entry, ArchitectureGraph graph) throws AmbigousException, EntryLoadingErrorException;
	
}
