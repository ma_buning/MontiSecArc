package secarc.ets.cocos.checkers;

import interfaces2.resolvers.AmbigousException;
import secarc._ast.ASTSecArcRole;
import secarc.ets.entries.RoleEntry;

/**
 * Context condition checker interface for checking role
 * related context conditions
 * 
 * <br>
 * <br>
 * Copyright (c) 2011 RWTH Aachen. All rights reserved
 * 
 * @author (last commit) $Author$
 * @version $Date$<br>
 * $Revision$
 * 
 */
public interface ISecRoleChecker {

	/**
	 * 
	 * @param node
	 * @param entry
	 * @throws AmbigousException
	 */
	void check(ASTSecArcRole node, RoleEntry entry) throws AmbigousException;
	
}
