package secarc.ets.cocos.checkers;

import interfaces2.resolvers.AmbigousException;
import secarc._ast.ASTSecArcFilter;
import secarc.ets.entries.FilterEntry;

/**
 * Context condition checker interface for checking filter
 * related context conditions
 * 
 * <br>
 * <br>
 * Copyright (c) 2011 RWTH Aachen. All rights reserved
 * 
 * @author (last commit) $Author$
 * @version $Date$<br>
 * $Revision$
 * 
 */
public interface ISecFilterChecker {

	/**
	 * 
	 * @param node
	 * @param entry
	 * @throws AmbigousException
	 */
	void check(ASTSecArcFilter node, FilterEntry entry) throws AmbigousException;
	
}
