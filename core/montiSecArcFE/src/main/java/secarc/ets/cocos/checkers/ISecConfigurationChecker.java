package secarc.ets.cocos.checkers;

import interfaces2.resolvers.AmbigousException;
import secarc._ast.ASTSecArcConfiguration;
import secarc.ets.entries.ConfigurationEntry;

/**
 * Context condition checker interface for checking configuration
 * related context conditions
 * 
 * <br>
 * <br>
 * Copyright (c) 2011 RWTH Aachen. All rights reserved
 * 
 * @author (last commit) $Author$
 * @version $Date$<br>
 * $Revision$
 * 
 */
public interface ISecConfigurationChecker {

	/**
	 * 
	 * @param node
	 * @param entry
	 * @throws AmbigousException
	 */
	void check(ASTSecArcConfiguration node, ConfigurationEntry entry) throws AmbigousException;	
	
}
