package secarc.ets.cocos.checkers;

import secarc._ast.ASTSecArcCPE;
import secarc.ets.entries.CPEEntry;

/**
 * Context condition checker interface for checking SecVersion
 * related context conditions
 * 
 * <br>
 * <br>
 * Copyright (c) 2011 RWTH Aachen. All rights reserved
 * 
 * @author (last commit) $Author$
 * @version $Date$<br>
 * $Revision$
 * 
 */
public interface ISecCPEChecker {

	/**
	 * 
	 * @param node ast node to be checked
	 * @param entry entry related version entry to be checked
	 */
	void check(ASTSecArcCPE node, CPEEntry entry);
	
}
