package secarc.ets.resolvers;

import secarc.ets.entries.TrustlevelEntry;
import mc.umlp.arcd.ets.resolvers.AbstractArcdResolverClient;
/**
* 
* Resolver client for trustlevel entries.
*
* <br>
* <br>
* Copyright (c) 2011 RWTH Aachen. All rights reserved.
*
* @author  (last commit) $Author:$
* @version $Date:$<br>
*          $Revision:$
*/
public class TrustlevelResolverClient extends AbstractArcdResolverClient {

	/* (non-Javadoc)
     * @see interfaces2.resolvers.IResolverClient#getResponsibleKind()
     */
	@Override
	public String getResponsibleKind() {
		return TrustlevelEntry.KIND;
	}

}
