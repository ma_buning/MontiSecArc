package secarc.ets.resolvers;

import mc.umlp.arcd.ets.resolvers.AbstractArcdResolverClient;
import secarc.ets.entries.CPEEntry;

/**
 * 
 * Resolver client for version entries.
 *
 * <br>
 * <br>
 * Copyright (c) 2011 RWTH Aachen. All rights reserved.
 *
 * @author  (last commit) $Author:$
 * @version $Date:$<br>
 *          $Revision:$
 */
public class CPEResolverClient extends AbstractArcdResolverClient {

	/* (non-Javadoc)
     * @see interfaces2.resolvers.IResolverClient#getResponsibleKind()
     */
	@Override
	public String getResponsibleKind() {
		return CPEEntry.KIND;
	}

}
