package secarc.ets.resolvers;

import secarc.ets.entries.TrustlevelRelationEntry;
import mc.umlp.arcd.ets.resolvers.AbstractArcdResolverClient;

/**
* 
* Resolver client for trustlevel relation entries.
*
* <br>
* <br>
* Copyright (c) 2011 RWTH Aachen. All rights reserved.
*
* @author  (last commit) $Author:$
* @version $Date:$<br>
*          $Revision:$
*/
public class TrustlevelRelationResolverClient extends
		AbstractArcdResolverClient {

	/*
	 * (non-Javadoc)
	 * @see interfaces2.resolvers.IResolverClient#getResponsibleKind()
	 */
	@Override
	public String getResponsibleKind() {
		return TrustlevelRelationEntry.KIND;
	}

}
