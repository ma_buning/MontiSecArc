package secarc.ets.resolvers;

import secarc.ets.entries.RoleEntry;
import mc.umlp.arcd.ets.resolvers.AbstractArcdResolverClient;

/**
 * 
 * Resolver client for componentRole entries.
 *
 * <br>
 * <br>
 * Copyright (c) 2011 RWTH Aachen. All rights reserved.
 *
 * @author  (last commit) $Author:$
 * @version $Date:$<br>
 *          $Revision:$
 */
public class RoleResolverClient extends AbstractArcdResolverClient {

	/* (non-Javadoc)
     * @see interfaces2.resolvers.IResolverClient#getResponsibleKind()
     */
	@Override
	public String getResponsibleKind() {
		return RoleEntry.KIND;
	}

}
