package secarc.ets.resolvers;

import secarc.ets.entries.ConfigurationEntry;
import mc.umlp.arcd.ets.resolvers.AbstractArcdResolverClient;

/**
 * 
 * Resolver client for configuration entries.
 *
 * <br>
 * <br>
 * Copyright (c) 2011 RWTH Aachen. All rights reserved.
 *
 * @author  (last commit) $Author:$
 * @version $Date:$<br>
 *          $Revision:$
 */
public class ConfigurationResolverClient extends AbstractArcdResolverClient {

	/* (non-Javadoc)
     * @see interfaces2.resolvers.IResolverClient#getResponsibleKind()
     */
	@Override
	public String getResponsibleKind() {
		return ConfigurationEntry.KIND;
	}

}
