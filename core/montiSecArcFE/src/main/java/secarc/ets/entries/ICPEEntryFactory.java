package secarc.ets.entries;

/**
 * Creates Creates {@link CPEEntry}s.
 *
 * <br>
 * <br>
 * Copyright (c) 2013 RWTH Aachen. All rights reserved.
 *
 * @author  (last commit) $Author$
 * @version $Date$<br>
 *          $Revision$
 */
public interface ICPEEntryFactory {

	/**
	 * Create a new {@link CPEEntry}
	 * 
	 * @return a new {@link CPEEntry}
	 */
	CPEEntry createCPE();
	
}
