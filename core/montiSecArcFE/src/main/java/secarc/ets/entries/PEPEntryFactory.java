package secarc.ets.entries;

import secarc.MontiSecArcConstants;

/**
 * 
 * Factory that created {@link PEPEntry}s.
 *
 * <br>
 * <br>
 * Copyright (c) 2012 RWTH Aachen. All rights reserved.
 *
 * @author  (last commit) $Author$
 * @version $Date$<br>
 *          $Revision$
 */
public class PEPEntryFactory implements IPEPEntryFactory{

	/* (non-Javadoc)
     * @see mc.umlp.arcd.ets.entries.IPEPEntryFactory#create()
     */
	@Override
	public PEPEntry createPEP() {
		PEPEntry entry = new PEPEntry();
		entry.setName(MontiSecArcConstants.PEP_NAME);
		
		return entry;
	}

}
