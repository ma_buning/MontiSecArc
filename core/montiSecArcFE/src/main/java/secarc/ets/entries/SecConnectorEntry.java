package secarc.ets.entries;

import mc.ets.serialization.STEntrySerializer.Serialized;
import mc.umlp.arcd.ArcdConstants;
import mc.umlp.arcd.ets.entries.ConnectorEntry;

/**
 * MontiSecArc connector entry.
 *
 * <br>
 * <br>
 * Copyright (c) 2013 RWTH Aachen. All rights reserved.
 *
 * @author  (last commit) $Author$
 * @version $Date$<br>
 *          $Revision$
 */
public class SecConnectorEntry extends ConnectorEntry {

	
	/**
	 * Maps encrypted to false
	 */
	@Serialized
	private boolean isEncrypted = false;
	
	/**
	 * @param encryption encrypted = true, unencrypted = false
	 */
	public void setEncryption(boolean encryption) {
		this.isEncrypted = encryption;
	}
	
	/**
	 * @return true, if the connection is encrypted, else false
	 */
	public boolean isEncrypted() {
		return isEncrypted;
	}
	
	/**
	 * @return true, if the connection is unencrypted, else false
	 */
	public boolean isUnencrypted() {
		return !isEncrypted;
	}
	
	/**
     * Default constructor. Creates a new {@link SecConnectorEntry}
     * with {@link ArcdConstants.ST_KIND_PROTECTED} visibility.
     */
	public SecConnectorEntry() {
		super();
	}
	
}
