package secarc.ets.entries;

import com.google.common.base.Preconditions;

/**
 * 
 * Factory that created {@link ConfigurationEntry}s.
 *
 * <br>
 * <br>
 * Copyright (c) 2012 RWTH Aachen. All rights reserved.
 *
 * @author  (last commit) $Author$
 * @version $Date$<br>
 *          $Revision$
 */
public class ConfigurationEntryFactory implements IConfigurationEntryFactory {

	/* (non-Javadoc)
	 * @see secarc.ets.entries.IConfigurationEntryFactory#createConfiguration()
	 */
	@Override
	public ConfigurationEntry createConfiguration() {
		return new ConfigurationEntry();
	}

	/* (non-Javadoc)
	 * @see secarc.ets.entries.IConfigurationEntryFactory#createConfiguration(java.lang.String)
	 */
	@Override
	public ConfigurationEntry createConfiguration(String name) {
		Preconditions.checkNotNull(name);
		
		ConfigurationEntry entry = createConfiguration();
		entry.setName(name);
		
		return entry;
	}

}
