package secarc.ets.entries;

import secarc.MontiSecArcConstants;

/**
 * Creates Creates {@link CPEEntry}s.
 *
 * <br>
 * <br>
 * Copyright (c) 2013 RWTH Aachen. All rights reserved.
 *
 * @author  (last commit) $Author$
 * @version $Date$<br>
 *          $Revision$
 */
public class CPEEntryFactory implements ICPEEntryFactory {

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.entries.ICPEEntryFactory#createCPE()
	 */
	@Override
	public CPEEntry createCPE() {
		CPEEntry entry = new CPEEntry();
		entry.setName(MontiSecArcConstants.CPE_NAME);
		
		return entry;
	}

}
