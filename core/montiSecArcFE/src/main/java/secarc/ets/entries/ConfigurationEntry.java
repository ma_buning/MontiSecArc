package secarc.ets.entries;

import secarc.MontiSecArcConstants;
import mc.umlp.arcd.ArcdConstants;
import mc.umlp.arcd.ets.entries.AbstractSTEntry;

/**
 * MontiSecArc configuration entry.
 *
 * <br>
 * <br>
 * Copyright (c) 2013 RWTH Aachen. All rights reserved.
 *
 * @author  (last commit) $Author$
 * @version $Date$<br>
 *          $Revision$
 */
public class ConfigurationEntry extends  AbstractSTEntry<ConfigurationEntry>{

	/**
     * Entry kind of configuration.
     */
    public static final String KIND = "SecArcConfiguration";
	
    /**
     * Default constructor. Creates a new {@link ConfigurationEntry}
     * with {@link ArcdConstants.ST_KIND_PROTECTED} visability.
     */
	public ConfigurationEntry() {
		super(ArcdConstants.ST_KIND_PROTECTED);
	}

	/*
     * (non-Javadoc)
     * @see interfaces2.STEntry#getLanguage()
     */
	@Override
	public String getLanguage() {
		return MontiSecArcConstants.MONTI_SEC_ARC_LANGUAGE_ID;
	}

	/*
     * (non-Javadoc)
     * @see interfaces2.STEntry#getKind()
     */
	@Override
	public String getKind() {
		return KIND;
	}

}
