package secarc.ets.entries;

/**
 * Creates Creates {@link IdentityEntry}s.
 *
 * <br>
 * <br>
 * Copyright (c) 2013 RWTH Aachen. All rights reserved.
 *
 * @author  (last commit) $Author$
 * @version $Date$<br>
 *          $Revision$
 */
public class IdentityEntryFactory implements IIdentityEntryFactory {

	/* (non-Javadoc)
	 * @see secarc.ets.entries.IIdentityEntryFactory#createIdentity()
	 */
	@Override
	public IdentityEntry createIdentity() {
		return new IdentityEntry();
	}

}
