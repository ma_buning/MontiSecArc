package secarc.ets.entries;

/**
 * Creates Creates {@link IdentityEntry}s.
 *
 * <br>
 * <br>
 * Copyright (c) 2013 RWTH Aachen. All rights reserved.
 *
 * @author  (last commit) $Author$
 * @version $Date$<br>
 *          $Revision$
 */
public interface IIdentityEntryFactory {

	/**
	 * Create a new {@link IdentityEntry}
	 * 
	 * @return a new {@link IdentityEntry}
	 */
	IdentityEntry createIdentity();
	
}
