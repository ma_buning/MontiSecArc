package secarc.ets.entries;

/**
 * 
 * Factory that created {@link TrustlevelRelationEntry}s.
 *
 * <br>
 * <br>
 * Copyright (c) 2012 RWTH Aachen. All rights reserved.
 *
 * @author  (last commit) $Author$
 * @version $Date$<br>
 *          $Revision$
 */
public class TrustlevelRelationEntryFactory implements ITrustlevelRelationEntryFactory{

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.entries.ITrustlevelRelationEntryFactory#createTrustlevelRelation()
	 */
	@Override
	public TrustlevelRelationEntry createTrustlevelRelation() {
		TrustlevelRelationEntry entry = new TrustlevelRelationEntry();
		return entry;
	}
	
}
