package secarc.ets.entries;

import mc.umlp.arc.ets.entries.MAComponentEntryFactory;
import mc.umlp.arcd.ets.entries.ComponentEntry;
import secarc.ets.entries.SecComponentEntry;

/**
 * 
 * Factory that created {@link SecComponentEntry}s.
 *
 * <br>
 * <br>
 * Copyright (c) 2012 RWTH Aachen. All rights reserved.
 *
 * @author  (last commit) $Author$
 * @version $Date$<br>
 *          $Revision$
 */
public class SecComponentEntryFactory extends MAComponentEntryFactory {

	/* (non-Javadoc)
     * @see mc.umlp.arcd.ets.entries.IComponentEntryFactory#create()
     */
    @Override
    public ComponentEntry createComponent() {
        return new SecComponentEntry();
    }
	
}
