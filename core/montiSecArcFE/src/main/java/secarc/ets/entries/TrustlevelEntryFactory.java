package secarc.ets.entries;

import secarc.MontiSecArcConstants;

/**
 * 
 * Factory that created {@link TrustlevelEntry}s.
 *
 * <br>
 * <br>
 * Copyright (c) 2012 RWTH Aachen. All rights reserved.
 *
 * @author  (last commit) $Author$
 * @version $Date$<br>
 *          $Revision$
 */
public class TrustlevelEntryFactory implements ITrustlevelEntryFactory {

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.entries.ITrustlevelEntryFactory#createTrustlevel()
	 */
	@Override
	public TrustlevelEntry createTrustlevel() {
		TrustlevelEntry entry = new TrustlevelEntry();
		entry.setName(MontiSecArcConstants.TRUSTLEVEL_NAME);
		return entry;
	}

}
