package secarc.ets.entries;

import mc.umlp.arcd.ets.entries.ConnectorEntry;
import mc.umlp.arcd.ets.entries.ConnectorEntryFactory;

/**
 * Creates Creates {@link SecConnectorEntry}s.
 *
 * <br>
 * <br>
 * Copyright (c) 2013 RWTH Aachen. All rights reserved.
 *
 * @author  (last commit) $Author$
 * @version $Date$<br>
 *          $Revision$
 */
public class SecConnectorEntryFactory extends ConnectorEntryFactory {

	/* (non-Javadoc)
     * @see mc.umlp.arcd.ets.entries.IPEPEntryFactory#create()
     */
	@Override
	public ConnectorEntry createConnector() {
		return new SecConnectorEntry();
	}

}
