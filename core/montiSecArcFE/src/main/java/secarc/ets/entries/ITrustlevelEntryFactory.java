package secarc.ets.entries;

/**
 * Creates Creates {@link TrustlevelEntry}s.
 *
 * <br>
 * <br>
 * Copyright (c) 2013 RWTH Aachen. All rights reserved.
 *
 * @author  (last commit) $Author$
 * @version $Date$<br>
 *          $Revision$
 */
public interface ITrustlevelEntryFactory {

	/**
	 * Create a new {@link TrustlevelEntry}
	 * 
	 * @return a new {@link TrustlevelEntry}
	 */
	TrustlevelEntry createTrustlevel();
	
}
