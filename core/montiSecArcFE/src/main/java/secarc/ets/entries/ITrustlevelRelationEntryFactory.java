package secarc.ets.entries;

/**
 * Creates Creates {@link TrustlevelRelationEntry}s.
 *
 * <br>
 * <br>
 * Copyright (c) 2013 RWTH Aachen. All rights reserved.
 *
 * @author  (last commit) $Author$
 * @version $Date$<br>
 *          $Revision$
 */
public interface ITrustlevelRelationEntryFactory {

	/**
	 * Create a new {@link TrustlevelRelationEntry}
	 * 
	 * @return a new {@link TrustlevelRelationEntry}
	 */
	TrustlevelRelationEntry createTrustlevelRelation();
	
}
