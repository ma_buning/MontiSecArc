package secarc.ets.entries;

/**
 * Creates Creates {@link RoleEntry}s.
 *
 * <br>
 * <br>
 * Copyright (c) 2013 RWTH Aachen. All rights reserved.
 *
 * @author  (last commit) $Author$
 * @version $Date$<br>
 *          $Revision$
 */
public interface IRoleEntryFactory {

	/**
	 * Create a new {@link RoleEntry}
	 * 
	 * @return a new {@link RoleEntry}
	 */
	RoleEntry createRole();
	
}
