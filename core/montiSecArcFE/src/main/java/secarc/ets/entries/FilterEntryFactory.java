package secarc.ets.entries;

import com.google.common.base.Preconditions;

/**
 * 
 * Factory that created {@link FilterEntry}s.
 *
 * <br>
 * <br>
 * Copyright (c) 2012 RWTH Aachen. All rights reserved.
 *
 * @author  (last commit) $Author$
 * @version $Date$<br>
 *          $Revision$
 */
public class FilterEntryFactory implements IFilterEntryFactory {

	/* (non-Javadoc)
     * @see mc.umlp.arcd.ets.entries.IFilterEntryFactory#create()
     */
	@Override
	public FilterEntry createFilter() {
		return new FilterEntry();
	}

	/* (non-Javadoc)
     * @see mc.umlp.arcd.ets.entries.IFilterEntryFactory#create(String name)
     */
	@Override
	public FilterEntry createFilter(String name) {
		Preconditions.checkNotNull(name);
		
		FilterEntry entry = createFilter();
		entry.setName(name);
		
		return entry;
	}

}
