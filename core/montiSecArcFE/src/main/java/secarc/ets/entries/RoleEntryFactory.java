package secarc.ets.entries;

/**
 * Creates Creates {@link RoleEntry}s.
 *
 * <br>
 * <br>
 * Copyright (c) 2013 RWTH Aachen. All rights reserved.
 *
 * @author  (last commit) $Author$
 * @version $Date$<br>
 *          $Revision$
 */
public class RoleEntryFactory implements IRoleEntryFactory {

	/* (non-Javadoc)
	 * @see secarc.ets.entries.IRoleEntryFactory#createRole()
	 */
	@Override
	public RoleEntry createRole() {
		return new RoleEntry();
	}

}
