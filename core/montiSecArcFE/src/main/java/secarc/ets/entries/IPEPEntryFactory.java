package secarc.ets.entries;

/**
 * Creates Creates {@link PEPEntry}s.
 *
 * <br>
 * <br>
 * Copyright (c) 2013 RWTH Aachen. All rights reserved.
 *
 * @author  (last commit) $Author$
 * @version $Date$<br>
 *          $Revision$
 */
public interface IPEPEntryFactory {

	/**
	 * Create a new {@link PEPEntry}
	 * 
	 * @return a new {@link PEPEntry}
	 */
	PEPEntry createPEP();
	
	
}
