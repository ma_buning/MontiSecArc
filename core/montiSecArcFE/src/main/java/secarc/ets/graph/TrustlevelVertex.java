package secarc.ets.graph;

import secarc.ets.entries.TrustlevelEntry;

/**
 * Vertex for a {@link Trustlevel}.
 * 
 * @author (last commit) $Author$
 * @version $Revision$, $Date$
 */
final class TrustlevelVertex extends Vertex<TrustlevelEntry> {

	/**
	 * Constructor for cc.clarc.lang.architecture.graph.TrustlevelVertex
	 * 
	 * @param architectureElementDescription
	 */
	  protected TrustlevelVertex(TrustlevelEntry architectureElementDescription) {
	    super(architectureElementDescription);
	  }
	
}
