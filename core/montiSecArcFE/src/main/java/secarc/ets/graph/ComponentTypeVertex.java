/* Copyright (c) 2012 RWTH Aachen. All rights reserved.
 * 
 * http://www.clarc.cc/ - http://www.se-rwth.de/ */
package secarc.ets.graph;

import mc.umlp.arcd.ets.entries.ComponentEntry;

/**
 * TODO: Write me!
 * 
 * @author (last commit) $Author$
 * @version $Revision$, $Date$
 * 
 */
final class ComponentTypeVertex extends Vertex<ComponentEntry> {
  
  /**
   * Constructor for cc.clarc.lang.architecture.graph.ComponentTypeVertex
   * 
   * @param architectureElementDescription
   */
  protected ComponentTypeVertex(ComponentEntry architectureElementDescription) {
    super(architectureElementDescription);
  }
  
}
