package secarc.ets.graph;

import secarc.ets.entries.FilterEntry;

/**
 * TODO: Write me!
 * 
 * @author (last commit) $Author$
 * @version $Revision$, $Date$
 * 
 */
final class FilterVertex extends Vertex<FilterEntry> {

	/**
	   * Constructor for cc.clarc.lang.architecture.graph.FilterVertex
	   * 
	   * @param architectureElementDescription
	   */
	protected FilterVertex(FilterEntry architectureElementDescription) {
		super(architectureElementDescription);
	}

}
