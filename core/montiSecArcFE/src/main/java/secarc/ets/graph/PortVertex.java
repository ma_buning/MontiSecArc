/* Copyright (c) 2012 RWTH Aachen. All rights reserved.
 * 
 * http://www.clarc.cc/ - http://www.se-rwth.de/ */
package secarc.ets.graph;

import mc.umlp.arcd.ets.entries.PortEntry;

/**
 * TODO: Write me!
 * 
 * @author (last commit) $Author$
 * @version $Revision$, $Date$
 * 
 */
final class PortVertex extends Vertex<PortEntry> {
  
  /**
   * Constructor for cc.clarc.lang.architecture.graph.PortVertex
   * 
   * @param architectureElementDescription
   */
  protected PortVertex(PortEntry architectureElementDescription) {
    super(architectureElementDescription);
  }
  
}
