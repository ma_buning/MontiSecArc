package secarc.ets.graph;

import secarc.ets.entries.IdentityEntry;

/**
 * TODO: Write me!
 * 
 * @author (last commit) $Author$
 * @version $Revision$, $Date$
 * 
 */
public class IdentityVertex extends Vertex<IdentityEntry>{

	/**
	 * Constructor for cc.clarc.lang.architecture.graph.IdentityVertex
	 * 
	 * @param architectureElementDescription
	 */
	protected IdentityVertex(IdentityEntry architectureElementDescription) {
	  super(architectureElementDescription);
	}
	
}
