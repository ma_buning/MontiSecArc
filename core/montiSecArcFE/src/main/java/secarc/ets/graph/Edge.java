/* Copyright (c) 2012 RWTH Aachen. All rights reserved.
 * 
 * http://www.clarc.cc/ - http://www.se-rwth.de/ */
package secarc.ets.graph;

import org.jgrapht.graph.DefaultEdge;

/**
 * Represents an edge in an {@link ArchitectureGraph}.
 * 
 * @author (last commit) $Author$
 * @version $Revision$, $Date$
 * 
 */
public class Edge extends DefaultEdge {
  
  private static final long serialVersionUID = 8312943492634005198L;
  
}
