/* Copyright (c) 2012 RWTH Aachen. All rights reserved.
 * 
 * http://www.clarc.cc/ - http://www.se-rwth.de/ */
package secarc.ets.graph;

import mc.umlp.arcd.ets.entries.SubComponentEntry;

/**
 * Vertex for a {@link SubComponent}.
 * 
 * @author (last commit) $Author$
 * @version $Revision$, $Date$
 */
final class ComponentVertex extends Vertex<SubComponentEntry> {
  
  /**
   * Constructor for cc.clarc.lang.architecture.graph.ComponentVertex
   * 
   * @param architectureElementDescription
   */
  protected ComponentVertex(SubComponentEntry architectureElementDescription) {
    super(architectureElementDescription);
  }
  
}
