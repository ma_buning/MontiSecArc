package secarc.ets.graph;

import secarc.ets.entries.RoleEntry;

/**
 * TODO: Write me!
 * 
 * @author (last commit) $Author$
 * @version $Revision$, $Date$
 * 
 */
public class RoleVertex extends Vertex<RoleEntry> {

	/**
	   * Constructor for cc.clarc.lang.architecture.graph.RoleVertex
	   * 
	   * @param architectureElementDescription
	   */
	  protected RoleVertex(RoleEntry architectureElementDescription) {
	    super(architectureElementDescription);
	  }
	
}
