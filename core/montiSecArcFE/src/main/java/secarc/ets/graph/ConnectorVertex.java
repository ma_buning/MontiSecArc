/* Copyright (c) 2012 RWTH Aachen. All rights reserved.
 * 
 * http://www.clarc.cc/ - http://www.se-rwth.de/ */
package secarc.ets.graph;

import mc.umlp.arcd.ets.entries.ConnectorEntry;

/**
 * TODO: Write me!
 * 
 * @author (last commit) $Author$
 * @version $Revision$, $Date$
 * 
 */
final class ConnectorVertex extends Vertex<ConnectorEntry> {
  
  /**
   * Constructor for cc.clarc.lang.architecture.graph.ConnectorVertex
   * 
   * @param architectureElementDescription
   */
  protected ConnectorVertex(ConnectorEntry architectureElementDescription) {
    super(architectureElementDescription);
  }
  
}
