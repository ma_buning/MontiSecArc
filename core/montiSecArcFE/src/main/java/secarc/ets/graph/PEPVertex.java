package secarc.ets.graph;

import secarc.ets.entries.PEPEntry;

/**
 * Vertex for a {@link PEP}.
 * 
 * @author (last commit) $Author$
 * @version $Revision$, $Date$
 */
public class PEPVertex extends Vertex<PEPEntry>{

	/**
	 * Constructor for cc.clarc.lang.architecture.graph.PEPVertex
	 * 
	 * @param architectureElementDescription
	 */
	 protected PEPVertex(PEPEntry architectureElementDescription) {
	    super(architectureElementDescription);
	 }
	
}
