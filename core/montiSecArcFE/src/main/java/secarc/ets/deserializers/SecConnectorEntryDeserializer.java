package secarc.ets.deserializers;

import static com.google.common.base.Preconditions.checkArgument;

import com.google.common.base.Optional;

import secarc.ets.entries.SecConnectorEntry;
import interfaces2.ISTEntry;
import mc.ast.ASTNode;
import mc.ets.serialization._ast.ASTObject;
import mc.umlp.arcd.ets.deserializers.ConnectorDeserializer;
import mc.umlp.arcd.ets.entries.ConnectorEntry;
import mc.umlp.arcd.ets.entries.IArcdEntryFactory;

/**
 * Deserializer for {@link ConnectorEntry}s. <br>
 * <br>
 * Copyright (c) 2011 RWTH Aachen. All rights reserved.
 * 
 * @author (last commit) $Author: $
 * @version $Date: $<br>
 *          $Revision: $
 */
public class SecConnectorEntryDeserializer extends ConnectorDeserializer {

	/**
	 * 
	 * @param arcdFactory
	 */
	public SecConnectorEntryDeserializer(IArcdEntryFactory arcdFactory) {
		super(arcdFactory);
	}
	
	/* (non-Javadoc)
     * @see interfaces2.loaders.STEntryDeserializer#deserialize(mc.ast.ASTNode)
     */
    @Override
    public ISTEntry deserialize(ASTNode node) {
        checkArgument(node instanceof ASTObject);
        ASTObject object = (ASTObject) node;
        
        SecConnectorEntry connector = (SecConnectorEntry) super.deserialize(node);
        
        Optional<Boolean> isEncrypted = object.getAttributeValue("isEncrypted");
        
        if(isEncrypted.isPresent()) {
        	connector.setEncryption(isEncrypted.get());
        }
        
        return connector;
    }

}
