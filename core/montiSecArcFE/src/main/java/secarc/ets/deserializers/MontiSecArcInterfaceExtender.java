package secarc.ets.deserializers;

import secarc.ets.entries.ConfigurationEntry;
import secarc.ets.entries.FilterEntry;
import secarc.ets.entries.IdentityEntry;
import secarc.ets.entries.PEPEntry;
import secarc.ets.entries.RoleEntry;
import secarc.ets.entries.TrustlevelEntry;
import secarc.ets.entries.CPEEntry;

import com.google.common.collect.ImmutableSet;

import mc.ets.serialization.STEntryDescriptionParserExtender;
import mc.umlp.arcd.ets.entries.ArcdFieldEntry;
import mc.umlp.arcd.ets.entries.ArcdTypeEntry;
import mc.umlp.arcd.ets.entries.ArcdTypeReferenceEntry;
import mc.umlp.arcd.ets.entries.ComponentEntry;
import mc.umlp.arcd.ets.entries.ConnectorEntry;
import mc.umlp.arcd.ets.entries.PortEntry;
import mc.umlp.arcd.ets.entries.SubComponentEntry;

/**
 * 
 * Parser extender for the MontiSecArc Symboltable.
 *
 * <br>
 * <br>
 * Copyright (c) 2011 RWTH Aachen. All rights reserved.
 *
 * @author  (last commit) $Author: ahaber $
 * @version $Date: 2013-07-11 16:43:12 +0200 (Thu, 11 Jul 2013) $<br>
 *          $Revision: 2424 $
 */
public class MontiSecArcInterfaceExtender extends STEntryDescriptionParserExtender {

	/**
     * @param entryKinds
     */
    public MontiSecArcInterfaceExtender() {
        super(ImmutableSet.of(
                ComponentEntry.KIND,
                SubComponentEntry.KIND,
                PortEntry.KIND,
                ConnectorEntry.KIND,
                ArcdFieldEntry.KIND,
                ArcdTypeEntry.KIND,
                ArcdTypeReferenceEntry.KIND,
                PEPEntry.KIND,
                CPEEntry.KIND,
                ConfigurationEntry.KIND,
                IdentityEntry.KIND,
                RoleEntry.KIND,
                FilterEntry.KIND,
                TrustlevelEntry.KIND));
    }
	
}
