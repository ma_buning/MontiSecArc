 /* Copyright (c) 2012 RWTH Aachen. All rights reserved.
  * 
  * http://www.clarc.cc/ - http://www.se-rwth.de/ */
package secarc._ast;
 
/**
 * {@link ASTList} for the respective production in MontiSecArc.mc
 * 
 * @author (last commit) $Author$
 * @version $Revision$, $Date$
 * 
 */
public class ASTSecArcSimpleConnectorList extends PrototypeASTSecArcSimpleConnectorList {

  /**
   * Constructor for cc.clarc.lang.architecture._ast.ASTSecArcConnectorList
   */
  public ASTSecArcSimpleConnectorList() {
	  super();
  }
 
  /**
   * Constructor for cc.clarc.lang.architecture._ast.ASTSecArcConnectorList
   */
   public ASTSecArcSimpleConnectorList(boolean strictlyOrdered) {
	   super(strictlyOrdered);
   }
     
}