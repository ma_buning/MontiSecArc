package secarc._ast;

import mc.ast.ASTNode;

/**
 * {@link ASTNode} for the respective production in MontiSecArc.mc
 * 
 * @author (last commit) $Author$
 * @version $Revision$, $Date$
 * 
 */
public class ASTSecArcTrustLevelList extends PrototypeASTSecArcTrustLevelList {

	/**
	 * Default constructor
	 */
	public ASTSecArcTrustLevelList() {
		super();
	}
	
	/**
     * Constructor for strictly ordered lists.
     * 
     * @param strictlyOrdered true, if this list should be strictly ordered.
     */
	public ASTSecArcTrustLevelList(boolean strictlyOrdered) {
		super(strictlyOrdered);
	}
	
}
