package secarc._ast;

import mc.ast.ASTNode;

/**
 * {@link ASTNode} for the respective production in MontiSecArc.mc
 * 
 * @author (last commit) $Author$
 * @version $Revision$, $Date$
 * 
 */
public class ASTSecArcConfigurationList extends
		PrototypeASTSecArcConfigurationList {

	/**
	 * Default constructor
	 */
	public ASTSecArcConfigurationList() {
		super();
	}
	
	/**
     * Constructor for strictly ordered lists.
     * 
     * @param strictlyOrdered true, if this list should be strictly ordered.
     */
	public ASTSecArcConfigurationList(boolean strictlyOrdered) {
		super(strictlyOrdered);
	}
	
}
