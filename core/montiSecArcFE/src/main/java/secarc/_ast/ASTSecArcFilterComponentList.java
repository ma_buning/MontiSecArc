package secarc._ast;

/**
 * {@link ASTNode} for the respective production in MontiSecArc.mc
 * 
 * @author (last commit) $Author$
 * @version $Revision$, $Date$
 * 
 */
public class ASTSecArcFilterComponentList extends PrototypeASTSecArcFilterComponentList {

	/**
	   * Constructor for cc.clarc.lang.architecture._ast.ASTSecArcFilterComponentList
	   */
	  public ASTSecArcFilterComponentList() {
		  super();
	  }
	 
	  /**
	   * Constructor for cc.clarc.lang.architecture._ast.ASTSecArcFilterComponentList
	   */
	   public ASTSecArcFilterComponentList(boolean strictlyOrdered) {
		   super(strictlyOrdered);
	   }
	
}
