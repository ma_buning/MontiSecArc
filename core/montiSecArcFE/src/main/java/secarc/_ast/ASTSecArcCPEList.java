package secarc._ast;

import mc.ast.ASTNode;

/**
 * {@link ASTNode} for the respective production in MontiSecArc.mc
 * 
 * @author (last commit) $Author$
 * @version $Revision$, $Date$
 * 
 */
public class ASTSecArcCPEList extends PrototypeASTSecArcCPEList {

	/**
	 * Default constructor
	 */
	public ASTSecArcCPEList() {
		super();
	}
	
	/**
     * Constructor for strictly ordered lists.
     * 
     * @param strictlyOrdered true, if this list should be strictly ordered.
     */
	public ASTSecArcCPEList(boolean strictlyOrdered) {
		super(strictlyOrdered);
	}
	
}
