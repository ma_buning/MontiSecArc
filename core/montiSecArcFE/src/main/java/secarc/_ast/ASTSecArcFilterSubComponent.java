package secarc._ast;

/**
 * {@link ASTNode} for the respective production in MontiSecArc.mc
 * 
 * @author (last commit) $Author$
 * @version $Revision$, $Date$
 * 
 */
public class ASTSecArcFilterSubComponent extends PrototypeASTSecArcFilterSubComponent{

}
