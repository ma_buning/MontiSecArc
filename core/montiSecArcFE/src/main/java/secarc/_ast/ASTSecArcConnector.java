/* Copyright (c) 2012 RWTH Aachen. All rights reserved.
 *  
 * http://www.clarc.cc/ - http://www.se-rwth.de/ */
package secarc._ast;
 
import java.util.List;
 
import mc.types._ast.ASTQualifiedName;

import com.google.common.collect.ImmutableList;
 
/**
 * {@link ASTNode} for the respective production in MontiSecARc.mc
 * 
 * @author (last commit) $Author$
 * @version $Revision$, $Date$
 * 
 */
public class ASTSecArcConnector extends PrototypeASTSecArcConnector {

	/**
     * @return the list of names of the targets of this connector.
     */
    public List<String> printTargets() {
 
	    ImmutableList.Builder<String> targetNames = ImmutableList.builder();
	 
	    for (ASTQualifiedName targetName : getTargets()) {
	    	targetNames.add(targetName.toString());
	    }
	  
	   return targetNames.build();
   }
     
}