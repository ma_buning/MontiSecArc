package secarc._ast;

import mc.ast.ASTNode;

/**
 * {@link ASTNode} for the respective production in MontiSecArc.mc
 * 
 * @author (last commit) $Author$
 * @version $Revision$, $Date$
 * 
 */
public class ASTSecArcRoleInterfaceList extends
		PrototypeASTSecArcRoleInterfaceList {

	/**
	 * Default constructor
	 */
	public ASTSecArcRoleInterfaceList() {
		super();
	}
	
	/**
     * Constructor for strictly ordered lists.
     * 
     * @param strictlyOrdered true, if this list should be strictly ordered.
     */
	public ASTSecArcRoleInterfaceList(boolean strictlyOrdered) {
		super(strictlyOrdered);
	}
	
}
