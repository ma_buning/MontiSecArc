package secarc._ast;

/**
 * {@link ASTNode} for the respective production in MontiSecArc.mc
 * 
 * @author (last commit) $Author$
 * @version $Revision$, $Date$
 * 
 */
public class ASTSecArcFilterSubComponentList extends PrototypeASTSecArcFilterSubComponentList{
	
	/**
	   * Constructor for cc.clarc.lang.architecture._ast.ASTSecArcFilterSubComponentList
	   */
	  public ASTSecArcFilterSubComponentList() {
		  super();
	  }
	 
	  /**
	   * Constructor for cc.clarc.lang.architecture._ast.ASTSecArcFilterSubComponentList
	   */
	   public ASTSecArcFilterSubComponentList(boolean strictlyOrdered) {
		   super(strictlyOrdered);
	   }

}
