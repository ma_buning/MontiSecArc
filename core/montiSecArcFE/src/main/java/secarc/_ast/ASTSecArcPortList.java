/* Copyright (c) 2012 RWTH Aachen. All rights reserved.
 * 
 * http://www.clarc.cc/ - http://www.se-rwth.de/ */
package secarc._ast;
 
/**
 * {@link ASTList} for the respective production in MontiSecArc.mc
 * 
 * @author (last commit) $Author$
 * @version $Revision$, $Date$
 * 
 */
public class ASTSecArcPortList extends PrototypeASTSecArcPortList {
   
	/**
     * Constructor for cc.clarc.lang.architecture._ast.ASTClArcMessagePortList
     */
    public ASTSecArcPortList() {
      super();
    }
    
    /**
     * Constructor for cc.clarc.lang.architecture._ast.ASTClArcMessagePortList
     */
    public ASTSecArcPortList(boolean strictlyOrdered) {
      super(strictlyOrdered);
    }
     
}